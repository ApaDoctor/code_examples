#!/usr/bin/env python3
"""
    This is the one of my projects, that i made for study.
    At this year.
    The task was to implement 3 functions:
        1) Checks checks the string has all braces correctly closed.
        2) Encrypts word with Caesar cipher. Takes second positional argument with list of shifts.
        3) The same as 2, but takes shift as additional arguments.

"""
import itertools


def balanced_paren(parenstr):
    """
    Function checks the string has all braces correctly closed.
    Conditions of correctly closed braces is:
        1. Count of closing and opening braces of each type are equal
        2. Braces closed in series reversed of opening.
           That means that, first brace can't be closed before second
           {[}] - wrong

    For example:
        '123', '[{}]', '12<4<[a]b>>5' are closed correctly.
        '{1<2(>3)}' - is closed wrong.

    :param parenstr: string that contains 0-n braces
    :return: True if all braces closed correctly and parenstr is string otherwise False
    """

    BRACES = {"(": 2, "[": 3, "{": 5, "<": 7, ")": 4, "]": 6, "}": 10, ">": 14}

    # parenstr is instance of str class
    if isinstance(parenstr, str):
        opened = []

        for x in parenstr:
            # skipping all not braces symbols
            if x not in BRACES:
                continue

            # if brace is opening - adding them to opened-braces stack
            if BRACES[x] in [2, 3, 5, 7]:
                opened.append(x)

            # if brace is closing
            else:
                # checking type if not the same that the last opened(we getting them and deleting) - return False
                if BRACES[opened.pop()] != BRACES[x] / 2:
                    return False

        # if count of opened braces in stack(that means that they aren't closed) more then zero
        # return False
        if len(opened) > 0:
            return False

        # if interpreter reached this instruction that means that everything is fine
        return True

    else:
        return False


def caesar_list(word, key=None):
    """
    Encrypts word with Caesar cipher.
    Shift listed in key param.
    For cases when word longer then key list - key list is looped, so for word "abcd" and key [2,] every letter
    will be shifted 2 times
    :param word: word that need to be encrypted
    :param key: positional argument that takes an iterable of integers, except string, default [1,2,3]
    :return: encrypted word with Caesar cipher
    """
    ASCII_START = 97
    # ASCII int code of char a
    RANGE = 26
    # Length of English alphabet excluding 1(a) + 1 (it's needed for correct return of modulo))

    # if key is None
    # or empty(for correct calling in caesar_varnumkey with defaults )
    if not key:
        key = [1, 2, 3]
    else:
        # if key contains not integer items
        for x in key:
            if not isinstance(x, int):
                raise TypeError("Key argument takes iterable of int values only")

    # checking type of word, if not in [a-z] and not str - raise TypeError
    if not (isinstance(word, str) and word.isalpha() and word.islower()):
        raise TypeError

    # using itertools to create infinite controlled generator
    key = itertools.cycle(key)

    # String that joined from generator, that makes shift of each letter
    # converting it into ASCII INT code and then making shift and then converting it conversely to char.
    # Shift is made of substracting minimal letter value and modulating them with max ASCII value(for our's range) + 1
    return "".join([chr((ord(x) - ASCII_START + next(key)) % RANGE + ASCII_START) for x in word])


def caesar_varnumkey(word, *args):
    """
    Encrypts word with Caesar cipher.
    Shift values can be defined as arguments after word
    Example: caesar_varnumkey("adsad", 1,2,3)
    For cases when word longer then shift arguments - arguments will be looped
    so in this way caesar_varnumkey("abcd", 2) every letter
    will be shifted 2 times

    :param word: word that need to be encrypted
    :param args: positional argument that takes an iterable of integers, default [1,2,3]
    :return: encrypted word with Caesar cipher
    """

    # calling caesar_list and sending list of arguments as second param
    return caesar_list(word, args)
