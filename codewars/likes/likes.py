import time


def likes(names):
    """
    :param names:  list of people who liked something
    :return: string that describes who liked something
    """
    n_length = len(names) or 1

    return {1: "%s likes this" % (names[0] if names else "no one"),
            2: "{} and {} like this",
            3: "{}, {} and {} like this",
            4: "{}, {} and {others} others like this"
            }[min(4, n_length)].format(*names[:3], others=n_length - 2)

