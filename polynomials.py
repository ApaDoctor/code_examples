#!/usr/bin/env python3

from collections import deque


class Polynomial:
    """
    Implementation of Polynomial in python.
    To create an instance need to set initial "x" values values.
    It can be done by this ways:
        Polynomial([1,-3,0,2])
        Polynomial(1,-3,0,2)
        Polynomial(x0=1,x3=2,x1=-3)
    This class can be used to compare, add,derivative,power polynomials.
    """

    def __init__(self, *args, **kwargs):
        """
        Constructor for object
        :param args: takes and list of х's or n-count of arguments for x
        :param kwargs: named arguments x0, x1 etc.
        """
        # if args set
        if args:
            # If exist list in the arguments making it the part of arguments
            args = sum(args, []) if isinstance(args[0], list) or isinstance(args[0], tuple) else args

            # moving args to kwargs dict (to have named x's)
            for x in range(0, len(args)):
                kwargs["x%s" % x] = args[x]
        self.raw_items = kwargs

        # Getting maximal x. If function max can't proceed - max_item = 0
        try:
            self.max_item = int(max(self.raw_items)[1:])
        except ValueError:
            self.max_item = 0

        # Creating list for any output
        self._gen_items()

    def get_items_list(self):
        """
        Method returns list of x's.
        :return: list of x's
        """
        return [self.raw_items["x%s" % x] for x in range(0, self.max_item + 1)]

    def _next_item_sign(self, ind):
        """
        Sign of the next item, after current with index ind
        :param ind: index of current item
        :return: sign of the next one
        """
        for x in range(0, ind + 1)[::-1]:
            item = self.raw_items.get("x%s" % (x - 1), 0)
            if item != 0:
                return "{:+d}".format(item)[:1]
        return ""

    def _gen_items(self):
        """
        Private method for generating list for output.
        """
        result = []
        for x in range(0, self.max_item + 1)[::-1]:
            # coof item should be without sign, sign we getting in previous iteration.
            coof = self.raw_items.get("x%s" % x, 0)
            coof = abs(coof) if x != self.max_item else coof
            # we don't need zero values to output
            if not coof:
                continue

            # if element is x0 - just adding coof of them to the output list
            # because we don't need to output x
            if x == 0:
                result.append(str(coof))

            # if it's the first x we need to output just them and x
            elif x == 1:
                result.append("%sx" % coof if coof > 1 else "x")
                # we need to get sign of next item
                next_sign = self._next_item_sign(x)
                # if next item exist(and sign too)) adding them to list
                if next_sign:
                    result.append(next_sign)

            # xn, n > 1
            else:
                result.append("%sx^%s" % (coof if coof > 1 else "", x))
                next_sign = self._next_item_sign(x)
                if next_sign:
                    result.append(next_sign)

        self.items = result

    def __str__(self):
        if self.items:
            # joining list of items for output with " "
            return " ".join(self.items)
        else:
            # if output list is empty - our polynomial is 0
            return "0"

    def __eq__(self, other):
        # to compare polynomials enough to compare their outputs
        return str(self) == str(other)

    def __add__(self, other):
        # it's impossible to add something different then polynomial
        # to polynomial (in this implementation)
        if not isinstance(other, Polynomial):
            raise TypeError("unsupported operand type(s) for +: '%s' and '%s'" % (type(self), type(other)))

        maximal = max(self.max_item, other.max_item)

        # to add 2 polynomials we need to add their cooficients
        return Polynomial([self.raw_items.get("x%s" % x, 0) + other.raw_items.get("x%s" % x, 0)
                           for x in range(0, maximal + 1)])

    def __mul__(self, other):
        # it's impossible to multiply with something different then polynomial
        # (in this implementation)
        if not isinstance(other, Polynomial):
            raise TypeError("unsupported operand type(s) for *: '%s' and '%s'" % (type(self), type(other)))

        result = []
        other_items_list = other.get_items_list()

        for i, v in enumerate(other_items_list):
            # using deque to increase powers of x  - rotate() method
            deq = deque(self.get_items_list() + [0] * other.max_item)
            deq.rotate(i)
            # multiplying multiplying other items
            result.append([x * v for x in deq])
        # and then we need just to sum
        return Polynomial([sum(x) for x in zip(*result)])

    def __pow__(self, power):
        # to power something we need just to multiply it with self for "power" times
        result = Polynomial(1)
        for i in range(power):
            result *= self
        return result

    def derivative(self):
        """
        Derivative Polynomial.
        :return: Polynomial after derivation
        """
        # derivation rule, multiplying power with x to get value
        return Polynomial([x * self.raw_items.get("x%s" % x, 0) for x in range(1, self.max_item + 1)])

    def at_value(self, x1, x2=None):
        """
        Value of Polynomial with defined x's
        If 2 values defined result is substraction of x'1 value and x'2 value
        :param x1:
        :param x2:
        :return:
        """
        if x2 is None:
            return sum([pow(x1, x) * self.raw_items.get("x%s" % x, 0) for x in range(0, self.max_item + 1)])
        else:
            return self.at_value(x2) - self.at_value(x1)


def test():
    print(Polynomial(0, 1, 0, -1, 4, -2, 0, 1, 3, 0), "\n3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x")
    assert str(Polynomial(0, 1, 0, -1, 4, -2, 0, 1, 3, 0)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
    assert str(Polynomial([-5, 1, 0, -1, 4, -2, 0, 1, 3, 0])) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x - 5"
    assert str(Polynomial(x7=1, x4=4, x8=3, x9=0, x0=0, x5=-2, x3=-1, x1=1)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
    assert str(Polynomial(x2=0)) == "0"
    assert str(Polynomial(x0=0)) == "0"
    assert Polynomial(x0=2, x1=0, x3=0, x2=3) == Polynomial(2, 0, 3)
    assert Polynomial(x2=0) == Polynomial(x0=0)
    assert str(Polynomial(x0=1) + Polynomial(x1=1)) == "x + 1"
    assert str(Polynomial([-1, 1, 1, 0]) + Polynomial(1, -1, 1)) == "2x^2"
    pol1 = Polynomial(x2=3, x0=1)
    pol2 = Polynomial(x1=1, x3=0)
    assert str(pol1 + pol2) == "3x^2 + x + 1"
    assert str(pol1 + pol2) == "3x^2 + x + 1"
    assert str(Polynomial(x0=-1, x1=1) ** 1) == "x - 1"
    assert str(Polynomial(x0=-1, x1=1) ** 2) == "x^2 - 2x + 1"
    pol3 = Polynomial(x0=-1, x1=1)
    assert str(pol3 ** 4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
    assert str(pol3 ** 4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
    assert str(Polynomial(x0=2).derivative()) == "0"
    assert str(Polynomial(x3=2, x1=3, x0=2).derivative()) == "6x^2 + 3"
    assert str(Polynomial(x3=2, x1=3, x0=2).derivative().derivative()) == "12x"
    pol4 = Polynomial(x3=2, x1=3, x0=2)
    assert str(pol4.derivative()) == "6x^2 + 3"
    assert str(pol4.derivative()) == "6x^2 + 3"
    assert Polynomial(-2, 3, 4, -5).at_value(0) == -2
    assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3) == 20
    assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3, 5) == 44
    pol5 = Polynomial([1, 0, -2])
    assert pol5.at_value(-2.4) == -10.52
    assert pol5.at_value(-2.4) == -10.52
    assert pol5.at_value(-1, 3.6) == -23.92
    assert pol5.at_value(-1, 3.6) == -23.92


if __name__ == '__main__':
    test()
